<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Simple extends CI_Controller {

   public function __construct()
   {
     parent::__construct();

     $this->load->model('simple_model');
   }

   public function index()
   {
      // localhost/ciexample/index.php/simple
      $this->load->view('simple/index');
   }

   public function get_url_param($param = '')
   {
      $data['param'] = $param;
      $this->load->view('simple/get_param_view', $data);
   }

   public function bootstrap()
   {
      $this->load->helper('url');
      $this->load->view('simple/bootstrap_view');
   }

   public function create_account()
   {
      $this->load->model('simple_model');

      $obj = new stdClass();
      $obj->status = false;

      $post_data = $this->input->post(NULL, TRUE);

      $username = $post_data['username'];
      $email    = $post_data['email'];
      $password = md5($post_data['password']);

      $create_status = $this->simple_model->create_account($username, $email, $password);

      if ($create_status === true) {
         $obj->status = true;
      }
      else {
         $obj->status = false;
      }

      echo json_encode($obj);
   }

   public function read_account()
   {
      $data['accounts'] = $this->simple_model->read_account();
      $this->load->view('simple/read_account_view', $data);
   }

   public function modify_account($id = '')
   {
      if ($id !== '') {
         $data['account'] = $this->simple_model->get_accout($id)[0];

         $this->load->view('simple/modify_account_view', $data);
      }
   }

   public function update_account()
   {
      $obj = new stdClass();
      $obj->status = false;

      $post_data = $this->input->post(NULL, TRUE);

      $username = $post_data['username'];
      $email    = $post_data['email'];
      $password = md5($post_data['password']);
      $id       = $post_data['id'];

      $update_status = $this->simple_model->update_account($username, $email, $password, $id);

      if ($update_status === true) {
         $obj->status = true;
      }
      else {
         $obj->status = false;
      }

      echo json_encode($obj);
   }

   public function all_data()
   {
      $data = $this->simple_model->read_account();
      echo json_encode($data);
   }

   public function delete_account()
   {
      $obj = new stdClass();
      $obj->status = false;

      $post_data = $this->input->post(NULL, TRUE);

      $id = $post_data['id'];

      $delete_result = $this->simple_model->delete_account($id);

      if ($delete_result === true) {
         $obj->status = true;
      }
      else {
         $obj->status = false;
      }

      echo json_encode($obj);
   }

 }

 /* End of file simple.php */
 /* Location: ./application/controllers/simple.php */