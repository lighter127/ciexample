<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simple_model extends CI_Model {

  // public $variable;

  public function __construct()
  {
    parent::__construct();
  }

  public function create_account($username, $email, $password)
  {
    // create method
    $data = array(
      'username' => $username,
      'email'    => $email,
      'password' => $password
    );

    if ($this->db->insert('user', $data)) {
      return true;
    }
    else {
      return false;
    }
  }

  public function read_account()
  {
    $query = $this->db->get('user');
    return $query->result_array();
  }

  public function get_accout($id)
  {
    $query = $this->db->get_where('user', array('id' => $id));
    return $query->result_array();
  }

  public function update_account($username, $email, $password, $id)
  {
    $data = array(
      'username' => $username,
      'email'    => $email,
      'password' => $password
    );

    $this->db->where(array('id' => $id));
    return $this->db->update('user', $data);
  }

  public function delete_account($id)
  {
    return $this->db->delete('user', array('id' => $id));
  }

}

/* End of file simple_model.php */
/* Location: ./application/models/simple_model.php */