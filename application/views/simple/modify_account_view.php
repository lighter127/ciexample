<!DOCTYPE html>
<html lang="en">
<head>
  <base href="<?=base_url()?>">
  <meta charset="UTF-8">
  <title>CI example with bootstrap</title>
  <script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
  <link href="<?=base_url()?>assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url()?>assets/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
  <script src="<?=base_url()?>assets/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

  <script type="text/javascript">
  $(document).ready(function(){

    // create account
    $('button#update').on('click', function(event){
      event.preventDefault();
      var username = $.trim($('input#username').val());
      var email    = $.trim($('input#email').val());
      var password = $.trim($('input#password').val());
      var id       = $.trim($('input#account_id').val());

      var post_url = 'simple/update_account';
      var post_data = {
        username: username,
        email: email,
        password: password,
        id: id
      };

      console.log(post_data);

      $.ajax({
        url: post_url,
        data: post_data,
        type: 'post',
        dataType: 'json',
        success: function(response) {
          console.log(response);
          if (response.status === true) {
            alert('update success');
          }
          else {
            alert('update failure');
          }
        },
        error: function(error) {
          console.log(error);
        }

      });
    });
  });
  </script>
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Show</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="simple/read_account">Read Account</a></li>
          <li><a href="simple/bootstrap">Create Account</a></li>
        </ul>
      </div><!--/.nav-collapse -->

    </div>
  </div>

  <div class="container">
    <div class="starter-template" style="position: relative;">

      <div class="row" style="margin-top: 5%">
        <div class="col-md-6 col-md-offset-3" style="text-align: left;">

          <table class="table table-bordered">
            <tbody>
                <tr>
                  <td>username</td>
                  <td>
                    <input type="text" id="username" class="form-control" value="<?=$account['username']?>">
                    <input type="hidden" id="account_id" value="<?=$account['id']?>">
                  </td>
                </tr>
                <tr>
                  <td>email</td>
                  <td>
                    <input type="text" id="email" class="form-control" value="<?=$account['email']?>">
                  </td>
                </tr>
                <tr>
                  <td>password</td>
                  <td>
                    <input type="text" id="password" class="form-control">
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <button type="button" class="btn btn-primary" id="update">update</button>
                  </td>
                </tr>
            </tbody>
          </table>

        </div>
      </div>

    </div>
  </div>
</body>
</html>