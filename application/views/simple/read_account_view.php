<!DOCTYPE html>
<html lang="en">
<head>
  <base href="<?=base_url()?>">
  <meta charset="UTF-8">
  <title>CI example with bootstrap</title>
  <script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
  <link href="<?=base_url()?>assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url()?>assets/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
  <script src="<?=base_url()?>assets/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

  <script type="text/javascript">
  $(document).ready(function(){
    $('button#delete').on('click', function(){
      var post_url = 'simple/delete_account';
      var post_data = {
        id: $(this).data('id')
      }

      $.ajax({
        url: post_url,
        data: post_data,
        type: 'POST',
        dataType: 'JSON',
        success: function(response) {
          if (response.status === true) {
            alert('delete success');
          }
          else {
            alert('delete failure');
          }
        }
      });
    });
  });
  </script>
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Show</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="simple/read_account">Read Account</a></li>
          <li><a href="simple/bootstrap">Create Account</a></li>
        </ul>
      </div><!--/.nav-collapse -->

    </div>
  </div>

  <div class="container">
    <div class="starter-template" style="position: relative;">

      <div class="row" style="margin-top: 5%">
        <div class="col-md-6 col-md-offset-3" style="text-align: left;">

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>number</th>
                <th>username</th>
                <th>email</th>
                <th>modify</th>
                <th>delete</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach ($accounts as $index => $account): ?>
                <tr>
                  <td><?=$index?></td>
                  <td><?=$account['username']?></td>
                  <td><?=$account['email']?></td>
                  <td>
                    <a href="simple/modify_account/<?=$account['id']?>" class="btn btn-primary">modify</a>
                  </td>
                  <td>
                    <button type="button" class="btn btn-danger" id="delete" data-id="<?=$account['id']?>">delete</button>
                  </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
          </table>

        </div>
      </div>

    </div>
  </div>
</body>
</html>